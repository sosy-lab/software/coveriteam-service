from flask import send_file
from pathlib import Path
from server.service import app
import pytest
import json
import time
from mock_server import MockServer

RESOURCES = Path(__file__).parent / "resources"


@pytest.fixture(scope="session")
def mock_server():
    server = MockServer()

    etag = 0

    def callback():
        nonlocal etag
        etag += 1
        return send_file(
            Path(__file__).parent / "resources" / "archive.zip",
            as_attachment=True,
            etag=str(etag),
        )

    server.app.add_url_rule("/archive.zip", view_func=callback)

    server.start()
    yield server
    server.stop()


@pytest.fixture(scope="function")
def cvt_request():
    return {
        "working_directory": "integration_test",
        "cvt_program": "many_actors.cvt",
        "coveriteam_inputs": {},
    }


@pytest.fixture()

# @pytest.mark.skip(reason="Temporarily disabled")
def test_rate_limiting(mock_server: MockServer, cvt_request):
    with app.test_client() as c:
        dummy = RESOURCES / "dummy.yml"
        verifier = RESOURCES / "many_actors.cvt"
        test = RESOURCES / "dummy.c"
        unreach_call = RESOURCES / "unreach-call.prp"
        t_start = time.time()
        response = c.post(
            "/execute",
            data={
                "args": json.dumps(cvt_request),
                "dummy.c": test.open("rb"),
                "many_actors.cvt": verifier.open("rb"),
                "dummy.yml": dummy.open("rb"),
                "unreach-call.prp": (unreach_call.open("rb")),
            },
        )
        t_diff = time.time() - t_start

        assert response.status_code == 200, "Response failed"
        assert t_diff >= 24, "Rate limiting should only allow 1 request every 2s"


def test_rate_limiting_single_actor(mock_server: MockServer, cvt_request):
    with app.test_client() as c:
        dummy = RESOURCES / "dummy.yml"
        verifier = RESOURCES / "single_actor.cvt"
        test = RESOURCES / "dummy.c"
        unreach_call = RESOURCES / "unreach-call.prp"
        response = c.post(
            "/execute",
            data={
                "args": json.dumps(cvt_request),
                "dummy.c": test.open("rb"),
                "many_actors.cvt": verifier.open("rb"),
                "dummy.yml": dummy.open("rb"),
                "unreach-call.prp": (unreach_call.open("rb")),
            },
        )

        assert response.status_code == 200, "Response failed"
