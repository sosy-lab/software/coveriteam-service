from flask import Flask
from werkzeug.serving import make_server
from threading import Thread
import requests
import time


class MockServer:
    def __init__(self, port=5000):
        self.port = port
        self.app = Flask(__name__)
        self.server = make_server("localhost", self.port, self.app)
        self.url = "http://localhost:%s" % self.port
        self.thread = None

        @self.app.route("/alive", methods=["GET"])
        def alive():
            return "True"

    def start(self):
        self.thread = Thread(target=self.server.serve_forever, daemon=True)
        self.thread.start()

        # Ensure server is alive before we continue running tests
        server_is_alive = False
        liveliness_attempts = 0
        while not server_is_alive:
            if liveliness_attempts >= 50:
                raise Exception(
                    "Failed to start and connect to mock server. "
                    f"Is port {self.port} in use by another application?"
                )
            liveliness_attempts += 1
            try:
                requests.get(self.url + "/alive", timeout=0.2)
                server_is_alive = True
            except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
            ):
                time.sleep(0.1)

    def stop(self):
        self.server.shutdown()
        self.thread.join()
