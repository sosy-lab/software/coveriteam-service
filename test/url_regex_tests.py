# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from server.vcloud_executor_util import ActorConfig
from nose.tools import assert_equal
from parameterized import parameterized
from itertools import product
import re


regexes = ActorConfig._ActorConfig__ALLOWED_URL_PREFIXES_FOR_ARCHIVES

FORBIDDEN_URLS = (
    "https://gitlab.com/sosy-lab/sv-comp/archives-2021/-/raw/master/2021/../../../../../archives-2020/-/raw/master/2020/cbmc.zip",  # noqa E501 line too long
    "https://gitlab.com/sosy-lab/sv-comp/archives-2021/uploads/0fc3425ee00b0265dfaa278d81694bc1/digraph.svg"  # noqa E501 line too long
    "https://gitlab.com/sosy-lab/sv-comp/archives-2019/-/raw/master/2019/2ls.zip",
    "https://zenodo.org/record/5720557",
)

forbidden_inputs = list(product(FORBIDDEN_URLS, regexes))
ALLOWED_URLS = (
    "https://gitlab.com/sosy-lab/sv-comp/archives-2020/-/raw/master/2020/2ls.zip",
    "https://gitlab.com/sosy-lab/test-comp/archives-2020/-/raw/master/2020/2ls.zip",
    "https://zenodo.org/api/files/34ae1b5f-8e98-4b6d-96a2-e7645fc20395/CPAchecker-2.1-unix.zip",  # noqa E501 line too long
)


@parameterized(forbidden_inputs)
def test_forbidden_urls(url, regex):
    """Never match URL"""
    assert not re.match(regex, url)  # noqa S101 Use of assert


@parameterized(ALLOWED_URLS)
def test_allowed_urls(url):
    """Match URL by exactly one of the regexes"""
    assert_equal(sum(True for regex in regexes if re.match(regex, url)), 1)
