<!--
This file is part of CoVeriTeam Remote:
https://gitlab.com/sosy-lab/software/coveriteam-remote-server

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->
# Development of CoVeriTeam-Remote Service

This document contains information how to set up a development and testing environment.

# Development Environment

This guide describes how to get from zero to working development environment

## Preliminaries

It is recommended to use a virtual environment for python, such that the installed packages do not interfere with you system packages.

For example you can use the built in `venv` module to create a lightweight environment.
```shell
python -m venv TestEnv # create a new environment in the folder TestEnv
source TestEnv/bin/activate # activate the environment
```

## Installation

There are are two repositories necessary to run the server: this one and benchexec. We guide you through installing them here.

### Step 1: Checkout
Clone this repository.
```shell
git clone git@gitlab.com:sosy-lab/software/coveriteam-remote-server.git
cd coveriteam-remote-server
```
Then you need to initialize the submodule:
```shell
git submodule init
git submodule update
```

### Step 2: Benchexec

The coveriteam-remote service server expects `benchexec` repo at `..`, i.e., if this repo is checked out at `DIR/abc` it needs `DIR/benchexec` to successfully execute.
To do this:
```shell
git clone git@github.com:sosy-lab/benchexec.git ../benchexec
```

### Step 3: Install Python Packages

If pip is available in your environment simply do:
```
pip install -r requirements.txt
```
Otherwise you'll need to search for the appropriate packages listed in the `requirements.txt` yourself.

## Running the Server

A test instance of the server can be started with:

```shell
python -m server.service
```
This will start the Flask development server on `127.0.0.1:5000`. 


The testsuite can be executed with:

```shell
python -m unittest discover -s test/
```

## Testing the Server

For testing the server you can use curl. To send formdata with curl you can modify the `example_request.json`.
```sh
curl -X POST -H "ContentType: multipart/form-data" --form args="$(cat doc/example_request.json)" 127.0.0.1:5000/execute
```

## Debugging with VSCode

If you wish to debug the app with VSCode do the following:

1. Open the Project in VSCode, i.e., with `code .` in the root folder of this repository
2. Configure the tests: 
    1. Hit `Ctrl+P`
    2. Type `Python: Configure Tests` and press `enter`
    3. Select `unittest`
    4. Select `test` as directory containing the tests
    5. Select `test*` as identify pattern
3. If you open the `test/tests.py` file you should now see a little play icon left of each test case. Upon right clicking you get the option to execute the test in debug mode.

