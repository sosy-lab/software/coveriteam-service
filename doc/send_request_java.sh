# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
#!/bin/bash

REMOTE="${1:-https://coveriteam-service.sosy-lab.org/execute}"
echo "POSTing to $REMOTE"
# Add option --insecure if client machine does not support HTTPS.
curl \
    --form "args=<example_request_java.json" \
    --form jbmc.yml=@data/jbmc.yml \
    --form Ackermann01=@data/Ackermann01.zip \
    --form common=@data/common.zip \
    --form assert_java.prp=@data/assert_java.prp \
    --form verifier-Java.cvt=@data/verifier-Java.cvt \
    --output cvt_remote_output.zip \
    $REMOTE
