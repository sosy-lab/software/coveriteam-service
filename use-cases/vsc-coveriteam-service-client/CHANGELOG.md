<!--
This file is part of CoVeriTeam Remote Service:
https://gitlab.com/sosy-lab/software/coveriteam-remote-server

SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Change Log

All notable changes to the "coveriteam-service-client" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [Unreleased]

- Initial release