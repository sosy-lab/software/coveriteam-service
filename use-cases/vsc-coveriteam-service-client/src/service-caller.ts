// This file is part of CoVeriTeam Remote Service:
// https://gitlab.com/sosy-lab/software/coveriteam-remote-server
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

import axios, { AxiosResponse } from 'axios';
import * as fs from "fs";
import * as FormData from 'form-data';
import * as JSZip from "jszip";
import * as path from "path";
import { printOnCoVeriTeamChannel } from './extension';
import * as vscode from 'vscode';

export async function performCheck(baseURL: string, input: CoVeriTeamInput) {
	const data = prepareInputForService(input);
	if (data) {
		const response = await callService(baseURL, data);
		await handleResponse(response);
	} else {
		vscode.window.showErrorMessage('NOT calling CoVeriTeam Service. Some input is missing!');
	}
}

export interface CoVeriTeamInput {
	descriptionFile: string,
	verifierPath: string,
	programPath: string,
	specificationPath: string,
	verifierVersion: string
}

function prepareInputForService(input: CoVeriTeamInput): FormData | undefined {
	const serviceData = new FormData();
	let getBaseName = (pathString: string) => path.parse(pathString).base;

	// https://github.com/axios/axios/issues/1006
	let appendFileToForm = (filePath: string, expectedInput: string) : boolean => {
		try {
			const knownLength = fs.statSync(filePath).size;
			serviceData.append(
				getBaseName(filePath),
				fs.createReadStream(filePath),
				{ filename: getBaseName(filePath), knownLength: knownLength });
			return true;
		} catch (error: any) {
			vscode.window.showErrorMessage(`No ${expectedInput}! Could not find ${filePath}!`);
			return false;
		}
	};

	serviceData.append("args", JSON.stringify(
		{
			/* eslint-disable @typescript-eslint/naming-convention */
			coveriteam_inputs: {
				verifier_path: getBaseName(input.verifierPath),
				program_path: getBaseName(input.programPath),
				specification_path: getBaseName(input.specificationPath),
				verifier_version: input.verifierVersion,
				data_model: "ILP32"
			},
			cvt_program: getBaseName(input.descriptionFile),
			working_directory: "coveriteam"
			/* eslint-enable @typescript-eslint/naming-convention */
		}
	));

	if (appendFileToForm(input.descriptionFile, "CoVeriTeam program")
		&& appendFileToForm(input.verifierPath, "verifier")
		&& appendFileToForm(input.specificationPath, "specification")
		&& appendFileToForm(input.programPath, "program")) { 

		return serviceData;
	}
	return undefined;
}

async function callService(baseURL: string, serviceData: FormData): Promise<AxiosResponse> {
	const cvtService = axios.create({ "baseURL": baseURL });

	let response;
	try {
		// make axios post request
		response = await cvtService.post("/execute", serviceData, {
			headers: {
				/* eslint-disable @typescript-eslint/naming-convention */
				"Content-Type": "multipart/form-data",
				// https://github.com/axios/axios/issues/1006
				"Content-Length": serviceData.getLengthSync()
				/* eslint-enable @typescript-eslint/naming-convention */
			},
			// NOTE: blob is only available in the browsers.
			responseType: "arraybuffer"
		});

	} catch (error: any) {
		console.error(error);
		displayErrorMessage();
		response = error.response;
	}
	return response;
}

async function handleResponse(resp: AxiosResponse<any, any>) {
	if (!resp) {
		vscode.window.showErrorMessage("Did not receive a response from CoVeriTeam Service."
			+" Please check your connection to the service!");
		const log = "No response from CoVeriTeam Service.";
		printOnCoVeriTeamChannel(log);
		return { "log" : log, "zip" : null };
	}
	if (resp.status !== 200) {
		vscode.window.showWarningMessage("Status from CoVeriTeam Service was not 200, it was " + resp.status + "!");
		let log;
		try {
			if (resp.data && resp.data.text) {
				const text = JSON.parse(resp.data.text);
				if (text.message) {
					log = text.message;
				} else {
					log = resp.data.text;
				}
			} else {
				log = "Something went wrong. There was no output.";
			}
		} catch (error) {
			console.error(error);
			displayErrorMessage();
			log = "An error occurred: " + resp.data.text;
		}
		printOnCoVeriTeamChannel(log);
		return { "log": log, "zip": null };
	}

	const zip = new JSZip();
	await zip.loadAsync(resp.data);
	const toolLog = await zip.file(/output/)[0].async("string");
	const coveriteamLog = await zip.file("LOG")?.async("string");
	let log = "";

	if (coveriteamLog) {
		log += `
--------------------------------------------------------------------------------
| CoVeriTeam's output:                                                         |
--------------------------------------------------------------------------------
\n`;
		log += coveriteamLog;
	}

	log += `
--------------------------------------------------------------------------------
| The executed command and the tool's output:                                  |
--------------------------------------------------------------------------------
\n`;
	log += toolLog;

	printOnCoVeriTeamChannel(log);
	return { "log": log, "zip": zip };
}

function displayErrorMessage() {
	vscode.window.showErrorMessage('Something went wrong!' 
		+ ' Refer to the "Window" output channel or the console' 
		+ ' (display it by executing the "Toggle Developer Tools" command).');
}
