// This file is part of CoVeriTeam Remote Service:
// https://gitlab.com/sosy-lab/software/coveriteam-remote-server
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as client from './service-caller';
import { CoVeriTeamInput } from './service-caller';
import * as path from "path";

declare global {
	type Blob = unknown;
}

const INPUT_DATA_DIR = path.join(__dirname, "../input-data");
// when changing this, also change the link in the settings (see package.json)
const DEFAULT_DESRIPTION = "verifier-C.cvt";
const DEFAULT_VERIFIER = "cbmc.yml";
const DEFAULT_SPECIFICATION = "unreach-call.prp";

let outputChannel: vscode.OutputChannel;

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	console.log('Congratulations, your extension "coveriteam-service-client" is now active!');
	outputChannel = vscode.window.createOutputChannel("CoVeriTeam Extension");

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('coveriteam-service-client.check', async () => {
		outputChannel.clear();
		outputChannel.show(true);
		
		let baseURL = vscode.workspace.getConfiguration('cvt').get('serviceURL') as string | undefined;
		if (!baseURL) {
			vscode.window.showErrorMessage('No CoVeriTeam Service URL set! Please adjust the extension settings.');
		}

		const partialInputData = prepareCoVeriTeamInputFromSettings();

		if (partialInputData && baseURL) {
			await client.performCheck(baseURL, partialInputData);

		} else {
			vscode.window.showErrorMessage('NOT calling CoVeriTeam Service. Some input is missing!');
		}
	});

	context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() { }

export const printOnCoVeriTeamChannel = (content: string | undefined): void => {
	if (content) {
		outputChannel.appendLine(content);
	}
};

function prepareCoVeriTeamInputFromSettings() : CoVeriTeamInput | undefined {
	let cvtFile = vscode.workspace.getConfiguration('cvt.input').get('cvtfile') as string | undefined;

	if (!cvtFile) {
		printOnCoVeriTeamChannel("Using the default CoVeriTeam program that is linked in the settings.");
		cvtFile = path.join(INPUT_DATA_DIR, DEFAULT_DESRIPTION);
	}
	if (!path.isAbsolute(cvtFile)) {
		vscode.window.showErrorMessage("The CoVeriTeam program path is not absolute! Please adjust the extension settings.");
		return undefined;
	}

	const programToVerify = getProgramToVerify();
	if (!programToVerify) {
		vscode.window.showErrorMessage(
			'No program to verify! Please open the program to verify and display it in the editor.');
		return undefined;
	}

	return {
		"descriptionFile": cvtFile,
		"programPath": programToVerify,
		// The following are hardcoded. At some point these can also be configurable.
		"verifierPath": path.join(INPUT_DATA_DIR, DEFAULT_VERIFIER),
		"specificationPath": path.join(INPUT_DATA_DIR, DEFAULT_SPECIFICATION),
		"verifierVersion": "default"
	};
}

function getProgramToVerify() {
	const editor = vscode.window.activeTextEditor;
	if (editor) {
		return editor.document.fileName;
	} else {
		return undefined;
	}
}
