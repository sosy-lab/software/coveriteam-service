<!--
This file is part of CoVeriTeam Remote Service:
https://gitlab.com/sosy-lab/software/coveriteam-remote-server

SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# coveriteam-service-client README

This extension would connect to the CoVeriTeam service to trigger the verification of the program in the current active editor.

## Development setup
The development enviroment requires NodeJS version > 16.
Run `npm install` to install the modules.

### Packaging
Install `vsce` using `npm install -g @vscode/vsce`, and then 
run `vsce package` to package the extension.