# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from hashlib import sha256
from fastapi import UploadFile


async def digest_file(file: UploadFile) -> str:

    sha = sha256()
    await file.seek(0)

    while data := await file.read(10 * 1024 * 1024):
        sha.update(data)

    await file.seek(0)

    return sha.hexdigest()
