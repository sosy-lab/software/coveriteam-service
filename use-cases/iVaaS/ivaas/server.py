# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from fastapi import FastAPI, UploadFile, Request
from fastapi.responses import HTMLResponse, Response, JSONResponse
from . import db
from .run_verification import run_verification
from .exceptions import VerifierException
import logging
from typing import Optional

logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")
app = FastAPI()


@app.post("/run")
async def run(
    program: UploadFile, specification: UploadFile, previous: Optional[UploadFile]
):
    prec = None
    if previous is not None:
        prec = await db.get_precision(previous, specification)
    else:
        logging.info("Found precision for previous program")

    if prec and prec.strip(b" \n"):
        logging.info(
            "Found non-empty precision:\n%s\n" + 10 * "-", prec.decode(errors="ignore")
        )

    (new_prec, log, cvt_response) = await run_verification(program, specification, prec)

    logging.info("CoVeriTeam Log:\n%s", log)

    await db.put_precision(program, specification, new_prec)

    # Grab ZIP file from in-memory, make response with correct MIME-type
    resp = Response(
        cvt_response,
        headers={
            "mimetype": "application/x-zip-compressed",
            "Content-Disposition": "attachment; filename=cvt-output.zip",
        },
    )
    return resp


@app.exception_handler(Exception)
async def verifier_exception_handler(request: Request, exc: VerifierException):
    return JSONResponse(
        status_code=500,
        content={"message": f"{exc}"},
    )


@app.get("/")
async def main():
    content = """
<body>
<form action="/run" enctype="multipart/form-data" method="post">
Specification: <input name="specification" type="file"><br/>
Program: <input name="program" type="file"><br/>
Previous: <input name="previous" type="file"><br/>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)
