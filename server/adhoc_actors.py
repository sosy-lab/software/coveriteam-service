# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import io
from dataclasses import dataclass
from pathlib import Path
from typing import List, Union

import requests

FM_TOOLS = (
    "https://gitlab.com/sosy-lab/benchmarking/fm-tools/-/raw/main/data/{tool}.yml"
)

StrPath = Union[str, Path]
FileRead = Union[io.BytesIO, StrPath]


@dataclass
class ActorDef:
    name: str
    tool_info: str
    options: List[str]


def download_fm_tools_yml(tool: str) -> io.BytesIO:
    link = FM_TOOLS.format(tool=tool)
    resp = requests.get(link)
    return resp.content.decode()


def get_actor_for(tool: str):
    return download_fm_tools_yml(tool=tool)
