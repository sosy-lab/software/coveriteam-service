# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0
import json
import logging
import os
from typing import Optional, Dict, Tuple

import yaml
import server.config as config
from server import CoVeriLangException
import requests
from pathlib import Path
import shutil
from zipfile import ZipFile, ZipInfo
import uuid
from server import InputValidationException
import re


# For result xml
RESULT_XML_PUBLIC_ID = "+//IDN sosy-lab.org//DTD BenchExec benchmark 2.3//EN"
RESULT_XML_SYSTEM_ID = "https://www.sosy-lab.org/benchexec/benchmark-2.3.dtd"

# URLs
_COVERITEAM_REPO = "https://gitlab.com/sosy-lab/software/coveriteam/-/raw/main/"
_BENCHMARKS_REPO = "https://gitlab.com/sosy-lab/benchmarking/sv-benchmarks/-/raw/main/"

# Resources
BENCHMARK_ELEM_DICT = {
    "tool": "coveriteam",
    "timelimit": "5 min",
    "hardtimelimit": "6 min",
    "memlimit": "8 GB",
    "cpuCores": "2",
}
REQUIRE_ELEM_DICT = {"cpuModel": "Intel Xeon E3-1230 v5 @ 3.40 GHz", "cpuCores": "2"}

_BENCHMARK_PY = str(config.INSTALL_DIR / "benchexec/contrib/vcloud-benchmark.py")
BENCHDEF_XML = "cvt-benchdef.xml"
BENCHEXEC_OUTPUT_PATH = "output/"


def get_command(tmpdir):
    cmd = [_BENCHMARK_PY, BENCHDEF_XML]

    if config.CLOUD_EXEC:
        cmd += ["--cgroupAccess"]
        cmd += ["--vcloudPriority", "URGENT"]
        cmd += ["--vcloudClientHeap", "300"]
        cmd += ["--no-compress"]
        cmd += ["--read-only-dir", "/"]
    else:
        # Flags for local execution
        cmd += ["--full-access-dir", "/sys/fs/cgroup"]
    cmd += ["--overlay-dir", str(tmpdir)]
    cmd += ["-o", BENCHEXEC_OUTPUT_PATH]
    return cmd


def __find_stem_for_benchmarks_repo(path, folder_name):
    # This function tries to the stem of the path that is possibly
    # a path in the benchmarks repo.
    pp = path.parts
    if folder_name in pp:
        i = len(pp) - pp[::-1].index(folder_name) - 1
        return "/".join(pp[i:])
    return None


def guess_file_url(path):
    """
    This function gueses the urls based on the file path.
    It is used for downloading required files from the
    coveriteam and benchmarks repositories.
    """
    if path.suffix == ".yml":
        # Actor definition file from the coveriteam repo
        return _COVERITEAM_REPO + "actors/" + path.name

    if path.suffix == ".cvt":
        # Coveriteam program from the coveriteam repo
        return _COVERITEAM_REPO + "examples/" + path.name

    # Program or specification paths from the benchmarks repo
    if __find_stem_for_benchmarks_repo(path, "c"):
        return _BENCHMARKS_REPO + __find_stem_for_benchmarks_repo(path, "c")
    if __find_stem_for_benchmarks_repo(path, "java"):
        return _BENCHMARKS_REPO + __find_stem_for_benchmarks_repo(path, "java")

    raise CoVeriLangException("Couldn't figure out URL for: " + str(path), 103)


def save_file_from_url(url, target):
    try:
        response = requests.get(url, allow_redirects=True)
        if response.status_code != 200:
            msg = "Couldn't get file from: %s. Server returned the code: %s" % (
                str(url),
                response.status_code,
            )
            raise CoVeriLangException(msg, 104)
        with target.open("wb") as out_file:
            out_file.write(response.content)
    except requests.ConnectionError:
        raise CoVeriLangException(
            "Connection error. Couldn't get file from : " + str(url), 104
        )


class ActorDefinitionLoader(yaml.SafeLoader):
    def ignore_include(self, node):
        return None


# Ignore !include
ActorDefinitionLoader.add_constructor("!include", ActorDefinitionLoader.ignore_include)


class ActorConfig:
    __ALLOWED_URL_PREFIXES_FOR_ARCHIVES = [
        # Competition repos
        r"^https://gitlab.com/sosy-lab/sv-comp/archives-202[01]/-/raw/master/202[01]/[\w-]+\.zip$",  # noqa E501
        r"^https://gitlab.com/sosy-lab/test-comp/archives-202[01]/-/raw/master/202[01]/[\w-]+.zip$",  # noqa E501
        # Competition repos from 2022 with branch named main (instead of master)
        r"^https://gitlab.com/sosy-lab/sv-comp/archives-202[23]/-/raw/main/202[23]/[\w-]+\.zip$",  # noqa E501
        r"^https://gitlab.com/sosy-lab/test-comp/archives-202[23]/-/raw/main/202[23]/[\w-]+.zip$",  # noqa E501
        # Trusted repos
        r"^https://gitlab.com/sosy-lab/benchmarking/coveriteam-archives/[\w-]+/-/raw/main/[\w-]+.zip$",  # noqa E501
        # Tool Info modules from the BenchExec repo
        r"^https://gitlab.com/sosy-lab/software/benchexec/-/raw/main/benchexec/tools/[\w-]+\.py$",  # noqa E501
        # Allow Zenodo URLs
        r"https://zenodo.org/api/files/[\w-]+/[\w.-]+.zip$",
    ]

    __ALLOWED_ZENODO_DOI_ARTIFACTS_IDS = {
        "5720557",
        "5898989",
    }

    def __init__(self, actor_def, version: str = ""):
        d = ActorConfig.__load_actor_def(actor_def)
        self.actor_name = d.get("actor_name", None)
        archive = ActorConfig.__get_tool_archive(
            actor_name=self.actor_name,
            archives=d.get("archives", None),
            version=version,
        )

        self.archive_location, archive_from_doi = ActorConfig.__get_archive_location(
            archive
        )
        self.version = archive.get("version") if archive else None
        self.tool_info = d.get("toolinfo_module", None)
        # Restriction: the file should either contain all: name, location, and tool info
        # or none
        config_fields = [self.actor_name, self.archive_location, self.tool_info]
        if not (all(config_fields) or not any(config_fields)):
            raise InputValidationException(
                "Received unexpected actor definition YAML file."
            )

        self.tool_dir = None
        self.archive_name = None
        if all(config_fields):
            self.archive_name = self.get_archive_name()
            # Keeping this path as str as it is going to be passed to vcloud
            if archive_from_doi:
                self.tool_dir = str(
                    config.CVT_CACHE_TOOLS / archive["doi"].replace("/", "-")
                )
            else:
                self.tool_dir = self.get_tool_installation_dir()
            self.__install_if_needed()
            self.__resolve_tool_info_module()

    @staticmethod
    def __get_tool_archive(actor_name: str, archives=Optional[list], version: str = ""):
        if not archives or len(archives) < 1:
            return None
        if version:
            possible_archive_list = [
                archive
                for archive in archives
                if str(archive.get("version", None)) == version
            ]
            if len(possible_archive_list) < 1:
                raise InputValidationException(
                    f"Did not find tool archive with "
                    f"version {version} in yaml of {actor_name}"
                )
            return possible_archive_list[0]

        logging.warning(
            f"No version specified. Using first version for actor {actor_name}!"
        )
        return archives[0]

    @staticmethod
    def __load_actor_def(yaml_file):
        with open(yaml_file, "r", encoding="utf8") as f:
            try:
                # It is indeed a safeloader.
                return yaml.load(f, ActorDefinitionLoader)  # noqa S506
            except yaml.YAMLError as e:
                msg = "Failed to load the YAML file: {} Error is: {}".format(
                    yaml_file, e
                )
                raise CoVeriLangException(msg, 202)

    @staticmethod
    def __get_archive_location(archive: Dict) -> Tuple[Optional[str], bool]:
        """
        Returns as string the URL where the tool archive is located.
        Returns also true, if the archive is given as DOI from Zenodo.
        """
        if not archive:
            return None, False

        if "location" in archive:
            if "doi" in archive:
                raise CoVeriLangException(
                    "DOI and location given for version %s. "
                    "Only one artifact location is allowed" % archive["version"]
                )
            return archive["location"], False

        if "doi" in archive:
            ActorConfig.__is_valid_zenodo_doi(archive["doi"])
            return ActorConfig.__resolve_zenodo_url(archive["doi"]), True

        raise CoVeriLangException(
            "No artifact location in archive for version %s" % archive["version"]
        )

    @staticmethod
    def __is_valid_zenodo_doi(doi_string: str):
        """
        Checks if the given string is a valid and downloadable DOI from ZENODO.
        If this method terminates, the string is valid
        """
        parts = doi_string.rsplit("/")
        if len(parts) != 2:
            raise CoVeriLangException(
                "Strange DOI! DOI %s is not allowed." % doi_string
            )

        second_part_splitted = parts[1].rsplit(".")
        if parts[0] != "10.5281" or second_part_splitted[0] != "zenodo":
            raise CoVeriLangException(
                "DOI %s is not allowed. Only Zenodo DOIs are allowed." % doi_string
            )

        if (
            second_part_splitted[1]
            not in ActorConfig.__ALLOWED_ZENODO_DOI_ARTIFACTS_IDS
        ):
            raise CoVeriLangException(
                "Artifact %s not allowed. Only the artifacts %s are allowed "
                % (
                    second_part_splitted[1],
                    ActorConfig.__ALLOWED_ZENODO_DOI_ARTIFACTS_IDS,
                )
            )

    @staticmethod
    def __resolve_zenodo_url(doi_string: str) -> str:
        """
        Returns the URL where the artifact for the given is located.
        """
        zenodo_api_url_base = "https://zenodo.org/api/records/"
        zenodo_record_id = doi_string.rsplit(".")[-1]
        url = zenodo_api_url_base + zenodo_record_id
        response = requests.get(url)
        data = json.loads(response.content)
        if len(data["files"]) > 1:
            raise CoVeriLangException(
                "More than one files are linked in this Zenodo record. "
                "The record should have only one file."
            )
        archive_url = data["files"][0]["links"]["self"]
        return archive_url

    def get_archive_name(self):
        archive_name = self.archive_location.rpartition("/")[2]
        if archive_name.endswith(".zip"):
            archive_name = archive_name.rpartition(".")[0]
        archive_name += ".zip"
        return archive_name

    def __install_if_needed(self):
        target_dir = Path(self.tool_dir)

        if not re.compile("^[A-Za-z0-9-._]+$").match(self.archive_name):
            raise CoVeriLangException(
                "Can't download tool because"
                "the version name contains invalid characters!",
                200,
            )

        archive_download_path = config.CVT_CACHE_ARCHIVES / self.archive_name
        downloaded = ActorConfig.__download_if_needed(
            self.archive_location, archive_download_path
        )

        # Unzip only if downloaded or the tool directory does not exist.
        if downloaded or not target_dir.is_dir():
            ActorConfig.__unzip(archive_download_path, target_dir)

    def get_tool_installation_dir(self):
        """
        I think it is easier for debugging if the dir name starts with the archive name.
        So, we take out the archive name and put it in the front instead of at the end.
        """
        archive_name = self.archive_name
        url_root = self.archive_location.rpartition("/")[0]
        if archive_name.endswith(".zip"):
            archive_name = archive_name.rpartition(".")[0]
        tool_dir_name = (
            archive_name + "-" + url_root.replace("://", "-").replace("/", "-")
        )
        return str(config.CVT_CACHE_TOOLS / tool_dir_name)

    def __resolve_tool_info_module(self):
        """
        1. Check if it is a URL.
        2. If a URL then download it and save it to the TI cache.
        3. Infer the module name and return it.
        """
        if ActorConfig.__is_url(self.tool_info):
            filename = config.CVT_CACHE_TOOL_INFO / self.tool_info.rpartition("/")[2]
            ActorConfig.__download_if_needed(self.tool_info, filename)
            self.tool_info = filename.name

    @staticmethod
    def __is_url(path_or_url):
        return "://" in path_or_url or path_or_url.startswith("file:")

    @staticmethod
    def __unzip(archive, target_dir):
        if target_dir.is_dir():
            shutil.rmtree(target_dir)

        unzip_folder = config.CVT_CACHE_TOOLS / str(uuid.uuid4())

        with ZipFile(archive, "r") as zipfile:
            top_folder = unzip_folder / zipfile.filelist[0].filename.split("/")[0]
            # Not using extract all as it does not
            # preserve the permission for executables.
            for member in zipfile.namelist():
                if not isinstance(member, ZipInfo):
                    member = zipfile.getinfo(member)
                extracted_file = zipfile.extract(
                    member, unzip_folder  # pytype: disable=wrong-arg-types
                )
                attr = member.external_attr >> 16
                if attr != 0:
                    os.chmod(extracted_file, attr)
            top_folder.rename(target_dir)

    @staticmethod
    def __download_if_needed(url, target):
        if not any(
            re.compile(allowed).match(url)
            for allowed in ActorConfig.__ALLOWED_URL_PREFIXES_FOR_ARCHIVES
        ):
            raise CoVeriLangException("Not allowed to download from this URL: %s" % url)

        headers = {}
        etag_path = Path(str(target) + ".etag")
        if etag_path.is_file() and target.is_file():
            with etag_path.open("r") as f:
                headers["If-None-Match"] = f.read()

        try:
            response = requests.get(url, allow_redirects=True, headers=headers)
            if response.status_code == 304:
                return False
            if response.status_code != 200:
                msg = (
                    "Couldn't download tool archive from: %s. "
                    + "Server returned the code: %s"
                ) % (str(url), response.status_code)
                raise Exception(msg)
            with target.open("wb") as out_file:
                out_file.write(response.content)
            # write the eta tag
            with Path(str(target) + ".etag").open("w") as f:
                f.write(response.headers["etag"])
            return True
        except requests.ConnectionError:
            raise CoVeriLangException("No network access.")
