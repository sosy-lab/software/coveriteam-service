# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import json
import logging
from json import JSONDecodeError

from flask import (
    Flask,
    abort,
    jsonify,
    request,
    send_from_directory,
    redirect,
    url_for,
    make_response,
)
from flask_cors import CORS

import server.config as config
from server import CoVeriLangException, InputValidationException, logutil
from server.contracts import InputDataContract
from server.local_executor import RequestProcessorLocal
from server.vcloud_executor import InputSanitizerVCloud
from server.adhoc_actors import get_actor_for

app = Flask(__name__, static_folder=config.STATIC)
CORS(app)

config.configure_logger(logging.getLogger())
logutil.setup()


@app.route("/execute", methods=["POST"])
def execute():

    logutil.log_request()

    if not request.files:
        msg = "Form data missing. No files were uploaded."
        logging.error(msg)
        abort(400, msg)

    if not request.form:
        msg = "Form data missing: Cannot run without arguments"
        logging.error(msg)
        abort(400, msg)

    raw_agrs = request.form["args"]

    # Can throw JSONDecoderError
    args = json.loads(raw_agrs)

    request_processor = get_request_processor(args, request.files)

    try:
        archive = request_processor.process_request()
        return send_from_directory(archive.parent, archive.name, as_attachment=True)
    finally:
        request_processor.cleanup()


@app.route("/actordef/<tool>")
def get_benchdef(tool: str):
    actor = get_actor_for(tool)
    response = make_response(actor, 200)
    response.mimetype = "text/plain"
    return response


@app.route("/")
def hello_world():
    logging.debug(url_for("static", filename="index.html"))
    return redirect(url_for("static", filename="index.html"))


@app.errorhandler(InputValidationException)
@app.errorhandler(CoVeriLangException)
@app.errorhandler(JSONDecodeError)
def handle_custom_exception(e):
    logging.error(str(e))
    response = jsonify({"message": str(e)})
    response.content_type = "application/json"
    response.status_code = 400
    return response


@app.errorhandler(Exception)
def handle_exception(e):
    logging.error(e)
    response = jsonify(
        {"message": "Execution failed! Please contact the support team."}
    )
    response.content_type = "application/json"
    response.status_code = 500
    return response


def get_request_processor(args, files):
    ip = InputDataContract(args, files)
    InputSanitizerVCloud(ip).sanitize_input()

    return RequestProcessorLocal(ip)


if __name__ == "__main__":
    app.config["DEBUG"] = config.DEBUG
    app.run()
