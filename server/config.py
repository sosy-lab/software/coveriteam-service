# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import logging
from pathlib import Path
import sys
from typing import Dict, cast
import toml

# TODO: Possibly catch load failure and silently use defaults

_toml_config = toml.load(Path(__file__).parent / "config.toml")
_config = _toml_config["config"]
_config = cast(Dict[str, str], _config)
_policies_dict = cast(Dict[str, Dict[str, bool]], _toml_config.get("policies", {}))

_policies = _policies_dict.get("service", {})
_benchexec_policies = _policies_dict.get("benchexec", {})

sys.dont_write_bytecode = True  # prevents writing .pyc files

script = Path(__file__).resolve()
INSTALL_DIR = script.parent.parent.parent


CVT_CACHE = (
    Path(_config["cache"]) if _config.get("cache", None) else Path.home() / ".cache"
) / "coveriteam"
# Directories
CVT_CACHE_ARCHIVES = CVT_CACHE / "archives"
CVT_CACHE_TOOLS = CVT_CACHE / "tools"
CVT_CACHE_TOOL_INFO = CVT_CACHE / "toolinfocache"
CVT_CACHE_RATE_LIMIT = CVT_CACHE / "ratelimit.db"
CVT_OUTPUT_DIR = _config.get("output_dir", "cvt-output")
# CoVeriTeam is present as a submodule
COVERITEAM_PATH = (
    script.parent / Path(_config["cvt_path"])
    if _config.get("cvt_path", None)
    else script.parent.parent / "coveriteam"
)
COVERITEAM_EXECUTABLE = COVERITEAM_PATH / "bin" / "__coveriteam_server_mode"

# Creat cache directories
CVT_CACHE_ARCHIVES.mkdir(parents=True, exist_ok=True)
CVT_CACHE_TOOLS.mkdir(parents=True, exist_ok=True)
CVT_CACHE_TOOL_INFO.mkdir(parents=True, exist_ok=True)

# Filename for the output archive
CVT_REMOTE_OUTPUT_ZIP: str = (
    _config.get("remote_output_zip", None) or "cvt_remote_output.zip"
)

# Are FileUploads Allowed?
ALLOW_FILE_UPLOADS = _policies.get("allow_file_upload", False)

# Flags to configure command for benchmark.py
CLOUD_EXEC = _benchexec_policies.get("cloud_exec", True)
DEBUG = _benchexec_policies.get("debug", False)

# Logger
LOGGING_LEVEL = _config.get("log_level", "INFO").upper()
LOGGING_OUTPUT = _config.get("log_output", "console")

EXTENDED_LOGGING: bool = cast(bool, _config.get("log_requests", False))
EXTENDED_LOG_DIR: Path = Path(_config.get("log_requests_dir", "requests"))

STATIC = script.parent.parent / "static"


def configure_logger(logger: logging.Logger) -> None:
    logger.setLevel(LOGGING_LEVEL)

    if LOGGING_OUTPUT == "console":
        return

    handler = logging.FileHandler(LOGGING_OUTPUT, mode="a", encoding="utf-8")
    logger.addHandler(handler)
