# SPDX-FileCopyrightText: 2022 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import logging
import shutil
import subprocess  # noqa: S404
import sys
import zipfile
from pathlib import Path
from typing import List

import werkzeug

import server.config as config
from server import CoVeriLangException
from server.request_processor_base import AbstractRequestProcessor


class RequestProcessorLocal(AbstractRequestProcessor):
    # TODO: check if this is still necessary
    def _prechecks(self):
        if not shutil.rmtree.avoids_symlink_attacks:  # pytype: disable=attribute-error
            sys.exit("I can't work like this. Deleting files are not secure.")

    def _create_required_files(self):
        target_path: str
        file_content: werkzeug.datastructures.FileStorage
        for target_path, file_content in self.input.files.items():
            fp = self.join_wd(Path(target_path))

            if target_path in self.input.zipped_directories:
                logging.debug(
                    f"Treating content received for {target_path} as zipped directory"
                )
                # file_content is a zip file. Extract its contents recursively to fp.
                fp.parent.mkdir(parents=True, exist_ok=True)
                with zipfile.ZipFile(file_content) as zip_ref:
                    zip_ref.extractall(fp.parent)
                logging.info(
                    f"Created Directory with content of {target_path}: {str(fp.parent)}"
                )
                if not fp.is_dir():
                    raise ValueError(
                        f"Zip file {target_path} does not contain directory {fp.name} as top-level element."
                    )

            else:
                fp.parent.mkdir(parents=True, exist_ok=True)
                logging.info(f"Created Directory: {str(fp.parent)}")
                file_content.save(fp)
                logging.info(f"Saved {fp.name} to {fp.parent}")

    def _assemble_cvt_command(self) -> List[str]:
        cvt_path = str(config.COVERITEAM_EXECUTABLE.resolve())

        base_command = [
            cvt_path,
            self.input.cvt_program,
            "--cache-dir",
            str(config.CVT_CACHE.resolve()),
            "--rate-limit",
            str(config.CVT_CACHE_RATE_LIMIT.resolve()),
            "--rename-from",
            # this file is constructed by the server in the working dir of the request
            "__cvts_internal_renaming.yml",
        ]
        input_statements = []
        for param, value in self.input.coveriteam_inputs.items():
            # If the same input param has invoked multiple times it
            # all values are collected in a list
            if isinstance(value, list):
                input_statements += ["--input", f"{param}={value.pop()}"]
                input_statements += sum(
                    (["--input", f"{param}+={val}"] for val in value), []
                )
            else:
                input_statements += ["--input", f"{param}={value}"]

        return base_command + input_statements

    def _execute_coveriteam(self):
        command = self._assemble_cvt_command()
        logging.info("Running coveriteam with: %s", command)
        output_path = self.join_wd(Path(config.CVT_OUTPUT_DIR))
        try:
            ret = subprocess.check_output(  # noqa: S603
                command, cwd=self._wd, stderr=subprocess.STDOUT
            )
        except subprocess.CalledProcessError as e:
            raise CoVeriLangException(
                "CoVeriTeam returned nonzero exit code: %s"
                % e.output.decode(errors="replace"),
                e.returncode,
            )
        try:
            logging.info("Coveriteam returned:\n%s", ret.decode(errors="replace"))
            log = output_path / "LOG"
            log.touch()
            log.write_bytes(ret)
        except FileNotFoundError as e:
            logging.error(f"The file {e.filename} was required but not found.")
            # CoVeriTeam failed to produce an output
            raise CoVeriLangException(
                "CoVeriTeam did not produce an expected output. Something went wrong:\n"
                f"{ret.decode(errors='replace')}"
            )

    def _prepare_response(self):
        archive = self.join_wd(config.CVT_REMOTE_OUTPUT_ZIP)
        output_path = self.join_wd(Path(config.CVT_OUTPUT_DIR))

        out = shutil.make_archive(
            str(archive.parent / archive.stem), "zip", output_path
        )
        return Path(out).resolve()
