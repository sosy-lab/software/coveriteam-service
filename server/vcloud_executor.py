# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import io
import logging
import os
import re
import shutil

# We use subprocess to only call benchmark.py, which is trusted.
import subprocess  # noqa S404 # nosec
import sys
from pathlib import Path
from typing import Dict

# We only create XMLs. We do not read any XML.
from xml.dom import minidom  # nosec # noqa S408 #pytype: disable=pyi-error
from xml.etree import ElementTree  # nosec # noqa S405
from xml.etree.ElementTree import Element, SubElement  # nosec # noqa S405
from zipfile import ZipFile

import yaml
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

import server.config as config
from server import CoVeriLangException, InputValidationException
from server.contracts import (
    AbstractInputSanitizer,
    InputDataContract,
    InputValidatorBase,
)
from server.request_processor_base import AbstractRequestProcessor
from server.vcloud_executor_util import (
    BENCHDEF_XML,
    BENCHEXEC_OUTPUT_PATH,
    BENCHMARK_ELEM_DICT,
    REQUIRE_ELEM_DICT,
    RESULT_XML_PUBLIC_ID,
    RESULT_XML_SYSTEM_ID,
    ActorConfig,
    get_command,
    guess_file_url,
    save_file_from_url,
)


class InputValidatorVCloud(InputValidatorBase):
    ALLOWED_INPUT_KEYS = {
        "program_path",
        "specification_path",
        "tester_path",
        "validator_path",
        "verifier_path",
        "tester_version",
        "validator_version",
        "verifier_version",
        "data_model",
    }
    ALLOWED_CVT_PROGRAMS = {"verifier.cvt", "tester.cvt", "validating-verifier.cvt"}

    def custom_validation(self):
        if self.input.cvt_program not in self.ALLOWED_CVT_PROGRAMS:
            raise InputValidationException(
                "Sorry, I only execute the following cvt programs: %s"
                % str(self.ALLOWED_CVT_PROGRAMS)
            )

        if self.input.working_directory != ".":
            raise InputValidationException(
                "Sorry, I do not allow this working directory."
            )
        # Check for the names of parameters to be passed to CoVeriTeam
        for param in self.input.coveriteam_inputs:
            if param not in self.ALLOWED_INPUT_KEYS:
                raise InputValidationException(
                    "This parameter name is not allowed: %s" % param
                )
        # Check that all values in the dict are None.
        if any(self.input.files.values()):
            raise InputValidationException("I am not accepting files today!")

    def check_allowed_file_path(self, fp):
        # TODO possibly a part of this should be moved to the base class.
        # 1. We do not allow access to the parent directory.
        # 2. Any part of the path can have following characters:
        #    alphanum and (_-+.)
        #    first and the last character should be alphanum

        path_parts = fp.parts
        # Check that path parts are not too many.
        if len(path_parts) > 7:
            raise InputValidationException("Too long a path.")

        part_name_regex = r"^\w([\-\+\.]*\w+)*$"
        for part in fp.parts:
            if part == "..":
                raise InputValidationException(
                    "Access to the parent directory in not allowed!"
                )
            if not re.match(part_name_regex, part):
                raise InputValidationException(
                    "The path %s is too fancy for me to handle!" % fp
                )


class InputSanitizerVCloud(AbstractInputSanitizer):
    """
    This should set the working directory to current working directory,
    and remove all leading '../' in the paths.
    """

    def __init__(self, input_data_contract: InputDataContract):
        super().__init__(input_data_contract)
        self.replacements: Dict[str, str] = {}

    def sanitize_input(self):
        super().sanitize_input()
        stream = io.BytesIO()
        yaml.dump(self.replacements, stream=stream, encoding="utf-8")
        stream.seek(0)
        renaming = FileStorage(
            stream,
            filename="__cvts_internal_renaming.yml",
            name="__cvts_internal_renaming.yml",
            content_type="application/octet-stream",
            content_length=len(stream.getvalue()),
        )
        self.input.files["__cvts_internal_renaming.yml"] = renaming

    def sanitize_working_directory(self):
        # just override it
        self.input.working_directory = "."

    def as_secure_name(self, name: str) -> str:
        if name in self.replacements:
            return self.replacements[name]

        new = secure_filename(name)
        if not new:
            new = hex(hash(name))[2:]
        self.replacements[name] = new
        return new

    def sanitize_coveriteam_inputs(self):
        # Remove leading reference to parent directories.

        new_inputs = {}
        for k, v in self.input.coveriteam_inputs.items():
            if isinstance(v, str):
                new_inputs[k] = self.as_secure_name(v)
            else:
                new_inputs[k] = [self.as_secure_name(vv) for vv in v]

        self.input.coveriteam_inputs = new_inputs

    def sanitize_cvt_program(self):
        # Get rid of leading ./ ../
        cvt_program = self.as_secure_name(self.input.cvt_program)
        self.input.cvt_program = Path(cvt_program)

    def sanitize_files(self):
        # Remove leading reference to parent directories.
        self.input.files = {
            self.as_secure_name(k): v for k, v in self.input.files.items()
        }


class RequestProcessorVCloud(AbstractRequestProcessor):
    def __init__(self, input_data_contract):
        self.version_map = {}
        super().__init__(input_data_contract=input_data_contract)

    def _prechecks(self):
        if not shutil.rmtree.avoids_symlink_attacks:  # pytype: disable=attribute-error
            sys.exit("I can't work like this. Deleting files are not secure.")

    def _create_required_files(self):
        # Mapping the given sanitized path to the given version, because
        # in __download_tools we need the version for an actor
        # and are looping through the file paths.

        # For example the value of tester_path will be mapped
        # to the value of tester_version
        mapping = {
            "tester_path": "tester_version",
            "validator_path": "validator_version",
            "verifier_path": "verifier_version",
        }

        # All version keys are present
        # Match tools with their Version
        self.version_map = {
            self.input.coveriteam_inputs[path_key]: self.input.coveriteam_inputs.get(
                mapping[path_key]
            )
            for path_key in mapping
            if path_key in self.input.coveriteam_inputs
        }

        for path in self.input.files.keys():
            url = guess_file_url(Path(path))
            p = self.join_wd(path)
            p.parent.mkdir(parents=True, exist_ok=True)
            save_file_from_url(url, p)

    def _execute_coveriteam(self):
        (required_tools, required_archives) = self.__download_tools()
        elem_benchmark = self.__create_benchdef_xml(required_tools, required_archives)
        self.__save_benchdef_xml(elem_benchmark)

        cmd = get_command(self._tmp)
        cvt_rel_path = os.path.relpath(str(config.COVERITEAM_PATH), self._wd)
        cmd += ["--tool-directory", cvt_rel_path]
        logging.debug("executing command: %s", cmd)
        try:
            # removing the proxy from evn (if present) for executing vcloud-benchmark.py
            env_without_proxy = os.environ.copy()
            env_without_proxy.pop("HTTP_PROXY", None)
            env_without_proxy.pop("HTTPS_PROXY", None)

            # Executing benchmark.py. It is trusted
            subprocess.run(  # nosec # noqa S603
                cmd, cwd=self._wd, env=env_without_proxy
            )
        except OSError:
            raise CoVeriLangException("Something went wrong. Can't say more!", 500)

    def __download_tools(self):
        required_tools = []
        required_archives = []
        for path in self.input.files.keys():
            path = self.join_wd(path)
            if path.suffix == ".yml":
                try:
                    actor = ActorConfig(
                        str(path.resolve()), self.__get_version_from_path(path)
                    )
                    if actor.tool_dir and actor.archive_location:
                        required_tools.append(actor.tool_dir)
                        required_archives.append(actor.archive_name)
                except CoVeriLangException as e:
                    if e.errorCode != 200:
                        raise e
        return required_tools, required_archives

    def __get_version_from_path(self, path):
        for k, v in self.version_map.items():
            if str(k) in str(path):
                return v
        return None

    def _prepare_response(self):
        archive = self.join_wd(config.CVT_REMOTE_OUTPUT_ZIP)
        op = Path(self.join_wd(BENCHEXEC_OUTPUT_PATH))
        coveriteam_output_sub_path = self.input.cvt_program + self._tmp
        coveriteam_output_glob = list(
            op.glob("*.files/" + coveriteam_output_sub_path + "/cvt-output")
        )
        log_file_glob = list(op.glob("*.logfiles/*.log"))
        # Raise an exception if log file was not produced.
        if len(log_file_glob) != 1:
            raise CoVeriLangException(
                "The execution did not produce desired result.", 500
            )

        logfile = log_file_glob[0]
        with ZipFile(self.join_wd(config.CVT_REMOTE_OUTPUT_ZIP), "w") as zipf:
            zipf.write(str(logfile.resolve()), "LOG")
            if len(coveriteam_output_glob) == 1:
                coveriteam_output_dir = coveriteam_output_glob[0]
                for root, _dirs, files in os.walk(coveriteam_output_dir):
                    for f in files:
                        filepath = os.path.join(root, f)
                        zipf.write(
                            filepath, os.path.relpath(filepath, coveriteam_output_dir)
                        )
            else:
                logging.warning(
                    "The execution did not produce exactly one output directory!"
                )

        return Path(archive).resolve()

    def __create_benchdef_xml(self, required_tools, required_archives):
        elem_benchmark = Element("benchmark", BENCHMARK_ELEM_DICT)
        SubElement(elem_benchmark, "require", REQUIRE_ELEM_DICT)
        SubElement(elem_benchmark, "resultfiles").text = "**"

        # Add required files.
        for f in self.input.files.keys():
            required_files_tag = SubElement(elem_benchmark, "requiredfiles")
            required_files_tag.text = f

        required_files_cache_tag = SubElement(elem_benchmark, "requiredfiles")
        required_files_cache_tag.text = str(config.CVT_CACHE_TOOL_INFO)

        # TODO remove archives when the offline flag is introduced
        for archive_name in required_archives:
            required_files_archives_tag = SubElement(elem_benchmark, "requiredfiles")
            required_files_archives_tag.text = str(
                config.CVT_CACHE_ARCHIVES / archive_name
            )

        for tool_dir in required_tools:
            required_files_archives_tag = SubElement(elem_benchmark, "requiredfiles")
            required_files_archives_tag.text = tool_dir

        options_tag = SubElement(elem_benchmark, "option", {"name": "--cache-dir"})
        options_tag.text = os.path.relpath(config.CVT_CACHE, self._wd)

        SubElement(elem_benchmark, "option", {"name": "--no-cache-update"})

        # Add inputs.
        for k, v in self.input.coveriteam_inputs.items():
            options_tag = SubElement(elem_benchmark, "option", {"name": "--input"})
            options_tag.text = k + "=" + v

        SubElement(elem_benchmark, "rundefinition")
        # Add Task
        elem_task = SubElement(
            elem_benchmark, "tasks", {"name": "CoVeriTeam-Remote-Task"}
        )
        include_tag = SubElement(elem_task, "include")
        include_tag.text = self.input.cvt_program

        return elem_benchmark

    def __save_benchdef_xml(self, xml_elem):
        xml_path = self.join_wd(BENCHDEF_XML)
        with xml_path.open("w", encoding="utf-8") as out_xml:
            rough_string = ElementTree.tostring(xml_elem, encoding="unicode")
            # We have created the string ourselves.
            reparsed = minidom.parseString(rough_string)  # noqa S318
            doctype = minidom.DOMImplementation().createDocumentType(
                "benchmark", RESULT_XML_PUBLIC_ID, RESULT_XML_SYSTEM_ID
            )
            reparsed.insertBefore(doctype, reparsed.documentElement)
            reparsed.writexml(
                out_xml, indent="", addindent="  ", newl="\n", encoding="utf-8"
            )
