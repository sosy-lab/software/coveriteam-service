# This file is part of CoVeriTeam Remote Service:
# https://gitlab.com/sosy-lab/software/coveriteam-remote-server
#
# SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

__version__ = "1.2-dev"


class InputValidationException(Exception):
    pass


class CoVeriLangException(Exception):
    """
    A bit on error codes:
        1**: failure in policy enforcement
        2**: yaml related errors
    """

    def __init__(self, msg, error_code=1):
        super().__init__(msg)
        self.errorCode = error_code
